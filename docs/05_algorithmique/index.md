---
author: Sophie CHANE-LUNE
title: Objectifs
---

# ALGORITHMIQUE

1. **Algorithme sur les arbres binaires**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Algorithmes sur les arbres binaires et sur les arbres binaires de recherche.
        - Calculer la taille et la hauteur d’un arbre.
        - Parcourir un arbre de différentes façons (ordres infixe, préfixe ou suffixe ; ordre en largeur d’abord).
        - Rechercher une clé dans un arbre de recherche, insérer une clé.

2. **Algorithme sur les graphes**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Algorithmes sur les graphes.
        - Parcourir un graphe en profondeur d’abord, en largeur d’abord.
        - Repérer la présence d’un cycle dans un graphe.
        - Chercher un chemin dans un graphe.

3. **Méthode « diviser pour régner »**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Méthode « diviser pour régner ».
        - Écrire un algorithme utilisant la méthode « diviser pour régner ».

4. **Programmation dynamique**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Programmation dynamique.
        - Utiliser la programmation dynamique pour écrire un algorithme.

5. **Algorithme de recherche textuelle**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Recherche textuelle.
        - Étudier l’algorithme de Boyer-Moore pour la recherche d’un motif dans un texte.