---
author: Sophie CHANE-LUNE
title: Objectifs
---

# LANGAGE & PROGRAMMATION

1. **Programmation Orientée Objet (POO)**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Vocabulaire de la programmation objet : classes, attributs, méthodes, objets.
        - Écrire la définition d’une classe.
        - Accéder aux attributs et méthodes d’une classe.

2. **Calculabilité**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Notion de programme en tant que donnée.
        - Calculabilité, décidabilité.
        - Comprendre que tout programme est aussi une donnée.
        - Comprendre que la calculabilité ne dépend pas du langage de programmation utilisé.
        - Montrer, sans formalisme théorique, que le problème de l’arrêt est indécidable.

3. **Récursivité**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Récursivité.
        - Écrire un programme récursif.
        - Analyser le fonctionnement d’un programme récursif.

4. **Modularité**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Modularité.
        - Utiliser des API (Application Programming Interface) ou des bibliothèques.
        - Exploiter leur documentation.
        - Créer des modules simples et les documenter.

5. **Paradigmes de programmation**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Paradigmes de programmation.
        - Distinguer sur des exemples les paradigmes impératif, fonctionnel et objet.
        - Choisir le paradigme de programmation selon le champ d’application d’un programme.

5. **Mise au point des programmes**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Mise au point des programmes.
        - Gestion des bugs.	
        - Dans la pratique de la programmation, savoir répondre aux causes typiques de bugs : problèmes liés au typage, effets de bord non désirés, débordements dans les tableaux, instruction conditionnelle non exhaustive, choix des inégalités, comparaisons et calculs entre flottants, mauvais nommage des variables, etc.
