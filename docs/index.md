# Lycée MHA

## Programme EDUSCOL
[Programme de NSI Terminale](https://eduscol.education.fr/document/30010/download){ .md-button target="_blank" rel="noopener"}

## Déroulement

1. [Programmation Orientée Objet (POO)](./04_langage-programmation/index.md)

## Blocs utilisés
!!! abstract "Définition"

!!! example "Exemple"

!!! tip "Ce qu'il faut savoir et savoir faire"

!!! warning "Remarque/Attention"

!!! quote "Citation"

!!! history "Histoire de l'informatique"

!!! video "Video"

!!! info "Remarque"

!!! note "Exercice"

!!! check "Solution/Correction"

!!! gear "Méthode/algorithme"

!!! code "Code/Programme"

!!! lien "Lien externe"

!!! capytale "Lien vers activité Capytale"