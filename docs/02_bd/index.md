---
author: Sophie CHANE-LUNE
title: Objectifs
---

# BASES DE DONNÉES 

1. **Modèle relationnel**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Modèle relationnel : relation, attribut, domaine, clef primaire, clef étrangère, schéma relationnel.
        - Identifier les concepts définissant le modèle relationnel.

2. **Base de données relationnelle**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Base de données relationnelle.
        - Savoir distinguer la structure d’une base de données de son contenu.
        - Repérer des anomalies dans le schéma d’une base de données.

3. **Système de gestion de bases de données relationnelles**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Système de gestion de bases de données relationnelles.
        - Identifier les services rendus par un système de gestion de bases de données relationnelles : persistance des données, gestion des accès concurrents, efficacité de traitement des requêtes, sécurisation des accès.

4. **SQL**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Langage SQL : requêtes d’interrogation et de mise à jour d’une base de données.
        - Identifier les composants d’une requête.
        - Construire des requêtes d’interrogation à l’aide des clauses du langage SQL : SELECT, FROM, WHERE, JOIN.
        - Construire des requêtes d’insertion et de mise à jour à l’aide de : UPDATE, INSERT, DELETE.