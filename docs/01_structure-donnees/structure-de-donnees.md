---
author: Sophie CHANE-LUNE
title: Objectifs
---

L'informatique est la science du traitement automatique des données. La quantité de ces données à traiter sont, de nos jours, de plus en plus importantes. Par conséquent, la façon dont ces données sont organisées et structurées est fondamentale pour obtenir un traitement efficace.  
A partir des types de base et/ou des types structurés (vus en première), on va pouvoir créer des structures de données plus complexes.

## I. Interface et implémentation
!!! info "Définition : Structure de données"

    Une **structure de données** est une manière de représenter, d'organiser, de stocker et de manipuler des données de manière efficace dans un programme informatique afin de pouvoir les traiter de façon simplifiée. 

Pour définir ces structures de données, on utilise des **types abstraits** de données (appelés aussi **structures de données abstraites**).

!!! info "Définition : Interface"

    Un type abstrait est caractérisé par une **interface** de programmation, c'est-à-dire un ensemble d'opérations accessible à un utilisateur de la structure de données, permettant de manipuler les données de ce type (par exemple ajouter, supprimer, accéder aux données).  
    **Spécifier** un type abstrait, c'est définir son interface, sans préjuger de la façon dont ses opérations seront implémentées.


!!! info "Définition : Implémentation"

    L’**implémentation** de la structure de données est la façon dont elle est représentée et codée en mémoire et n’est pas forcément accessible à l’utilisateur.  
    **Implémenter** un type abstrait, c'est fournir le code de ces opérations en utilisant des types existants.  

!!! warning "Remarque"

    Plusieurs implémentations peuvent répondre à la même spécification.


```
class Liste:
    def __init__(self):
        """
        Initialise une liste vide.
        """

    def est_vide(self):
        """
        Vérifie si la liste est vide.
        Retourne True si la liste est vide, sinon False.
        """

    def longueur(self):
        """
        Retourne la longueur (nombre d'éléments) de la liste.
        """

    def ajouter(self, element):
        """
        Ajoute un élément à la fin de la liste.
        """

    def inserer(self, element, index):
        """
        Insère un élément à l'index spécifié.
        """

    def supprimer(self, element):
        """
        Supprime la première occurrence de l'élément donné de la liste.
        Si l'élément n'est pas trouvé, ne fait rien.
        """

    def retirer(self, index):
        """
        Supprime l'élément à l'index spécifié de la liste.
        """

    def obtenir(self, index):
        """
        Retourne l'élément à l'index spécifié.
        """

    def index_de(self, element):
        """
        Retourne l'index de la première occurrence de l'élément donné.
        Si l'élément n'est pas trouvé, retourne -1.
        """

    def afficher(self):
        """
        Affiche les éléments de la liste.
        """
```