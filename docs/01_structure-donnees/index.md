---
author: Sophie CHANE-LUNE
title: Objectifs
---

# STRUCTURES DE DONNÉES 

1. **Structure de données**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Structures de données, interface et implémentation.
        - Spécifier une structure de données par son interface.
        - Distinguer interface et implémentation.
        - Écrire plusieurs implémentations d’une même structure de données.

2. **Structures linéaires**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Listes, piles, files : structures linéaires.
        - Dictionnaires, index et clé.
        - Distinguer des structures par le jeu des méthodes qui les caractérisent.
        - Choisir une structure de données adaptée à la situation à modéliser.
        - Distinguer la recherche d’une valeur dans une liste et dans un dictionnaire.

3. **Structures hiérarchiques**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Arbres : structures hiérarchiques.
        - Arbres binaires : nœuds, racines, feuilles, sous-arbres gauches, sous-arbres droits.
        - Identifier des situations nécessitant une structure de données arborescente.
        - Évaluer quelques mesures des arbres binaires (taille, encadrement de la hauteur, etc.).

4. **Structures relationnelles**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Graphes : structures relationnelles.
        - Sommets, arcs, arêtes, graphes orientés ou non orientés.
        - Modéliser des situations sous forme de graphes.
        - Écrire les implémentations correspondantes d’un graphe : matrice d’adjacence, liste de successeurs/de prédécesseurs.
        - Passer d’une représentation à une autre.