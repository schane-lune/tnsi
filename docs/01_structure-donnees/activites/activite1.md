---
author: Sophie CHANE-LUNE
title: Activité 1 Papier & crayon !
---

#### Partie 1
On donne la séquence d'instructions suivante :
```script
L = creer_liste()
inserer(L,"A",1)
inserer(L,"O",2)
inserer(L,"B",1)
inserer(L,"V",3)
inserer(L,"R",2)
```
Illustrer le résultat de chaque étape de cette séquence.

#### Partie 2
On donne la séquence d'instructions suivante :
```
L1 = creer_liste()
L2 = creer_liste()
inserer(L1,1,1)
inserer(L1,2,2)
inserer(L1,3,3)
inserer(L1,4,4)
inserer(L2,lire(L1,1),1)
inserer(L2,lire(L1,2),1)
inserer(L2,lire(L1,3),1)
inserer(L2,lire(L1,4),1)
```
1. Illustrer le résultat de chaque étape de cette séquence.  
2. Quelle est l'opération effectuée ?