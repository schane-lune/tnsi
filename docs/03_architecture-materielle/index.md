---
author: Sophie CHANE-LUNE
title: Objectifs
---

# ARCHITECTURE MATÉRIELLE & SYSTÈME D'EXPLOITATION

 1. **Système sur puce**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Composants intégrés d’un système sur puce.
        - Identifier les principaux composants sur un schéma de circuit et les avantages de leur intégration en termes de vitesse et de consommation.

2. **Gestion des processus**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Gestion des processus et des ressources par un système d’exploitation.
        - Décrire la création d’un processus, l’ordonnancement de plusieurs processus par le système.
        - Mettre en évidence le risque de l’interblocage (deadlock).

3. **Protocoles de routage**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Protocoles de routage.
        - Identifier, suivant le protocole de routage utilisé, la route empruntée par un paquet.

4. **Sécurisation des communications**

    !!! tip "Ce qu'il faut savoir et savoir faire"

        - Sécurisation des communications.
        - Décrire les principes de chiffrement symétrique (clef partagée) et asymétrique (avec clef privée/clef publique).
        - Décrire l’échange d’une clef symétrique en utilisant un protocole asymétrique pour sécuriser une communication HTTPS.
